const fs = require('fs');

fs.readFile('./data/events.txt', 'utf8', (err, data) => {
  if (err) throw err;
  // console.log(data);

  // console.log(data.split("\n"))
  var output = "const Events = "

  list = []
  data.split("\n").forEach((line) =>{
    if (line !== ""){
      var res = line.split("\t")
      console.log(res[0])
      var row = {}
      row.start = parseFloat(res[0])
      row.end = parseFloat(res[1])
      row.tag = res[2]
      list.push(row)
    }
  })

  output += JSON.stringify(list)

  fs.writeFile('./public/data/events.js', output, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
});
