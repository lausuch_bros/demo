'use strict';

// class Loading extends Demo{
//   constructor(){
//     super([
//       new LoadingScene()
//     ])
//   }
// }

class TheDemo extends Demo{
  constructor(){
    super([
      new InitLogoScene(),
      new CubeFieldScene(),
      new Tunnel_Scene(),
      new Beat_Scene(),
      new Particles_Scene(),
      new End_Scene()
    ])
  }

  onEvent(event, time, onStart){
    switch (event.tag) {
      case "scene1:init":
      case "scene2:init":
      case "scene2:end":
      case "scene4:init":
      case "end":
        this.next_scene()
      break;
    }
  }

  start(){
    //this.music.offset = 22.327829 // Scene1
    //this.music.offset = 63.590496 // Scene2
    //this.music.offset = 107.09575 // Scene4
    //this.music.offset = 114.882293 // Scene4
    //this.music.offset = 169.74122 // end
    this.music.play()
    this.timerController.setOffset(this.music.offset)
    super.start()
  }
}
//
// var loading = new Loading()
// loading.setup()
// loading.start()

var demo = new TheDemo()
demo.preload(()=>{
  demo.setup()
  showRun()
  //demo.start()
})
