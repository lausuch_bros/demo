class InitLogoScene extends Scene {
  preload(sync) {
    sync.addTask((sync) =>{
      new THREE.OBJLoader2().load( './data/logo.obj', ( event )=> {
        this.logo = event.detail.loaderRootNode
        sync.ready();
      }, null, null, null, false );
    })

    sync.addTask((sync) =>{
      new THREE.FontLoader().load( 'data/helvetiker_bold.typeface.json',  ( font )=> {
        var xMid, text;
        var color = new THREE.Color( 0x00000 );
        var matDark = new THREE.MeshBasicMaterial( {
          color: color,
          side: THREE.DoubleSide
        } );
        var matLite = new THREE.MeshBasicMaterial( {
          color: color,
          transparent: false,
          side: THREE.DoubleSide
        } );

        function generate_text(message, scale = 0.3){
          var shapes = font.generateShapes( message, 100 );
          var geometry = new THREE.ShapeBufferGeometry( shapes );
          geometry.computeBoundingBox();
          xMid = - 0.5 * ( geometry.boundingBox.max.x - geometry.boundingBox.min.x );
          geometry.translate( xMid, 0, 0 );
          text = new THREE.Mesh( geometry, matLite );

          text.visible = false
          text.position.y = 500
          text.position.x = 0
          text.position.z = -100
          var scale = scale
          text.scale.x = scale
          text.scale.y = scale
          return text
        }

        this.text1 = generate_text("Hackweek\n     2019", 0.26);
        this.text2 = generate_text("Lausuch\nBrothers");
        this.text3 = generate_text("Code &\n Music");

        sync.ready();
      })
    })
  }

  setup() {
    this.scene.background = new THREE.Color(0x0);
    this.directionalLight = new THREE.DirectionalLight(0xffccaa,3);
    this.directionalLight.position.set(0,0,-1);
    this.scene.add(this.directionalLight);

    this.logoLight = new THREE.PointLight( 0x00ff00, 1, 10 );
    this.logoLight.position.set(0,0,-1);
    var sphere = new THREE.SphereBufferGeometry( 0.5, 16, 8 );
    this.logoLight.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0x00ff00 } ) ) );
    this.scene.add( this.logoLight );
    this.logoLight.visible = false;
    this.logoLight.intensity = 0

    var circleGeo = new THREE.CircleGeometry(220,50);
    var circleMat = new THREE.MeshBasicMaterial({color: 0xffccaa});
    var circle = new THREE.Mesh(circleGeo, circleMat);
    circle.position.set(0 , 500 ,-500);
    circle.scale.setX(1.2);

    this.scene.add(circle);
    this.scene.add(this.logo);
    this.scene.add(this.text1);
    this.scene.add(this.text2);
    this.scene.add(this.text3);

    {
      this.logo.position.x = 0
      this.logo.position.y = 510
      this.logo.position.z = 100
      var scale  = 1500
      this.logo.scale.x = scale
      this.logo.scale.y = scale
      this.logo.scale.z = scale
      this.logo.rotation.x = 3.14/2
    }

    var areaImage = new Image();
    areaImage.src = POSTPROCESSING.SMAAEffect.areaImageDataURL;
    var searchImage = new Image();
    searchImage.src = POSTPROCESSING.SMAAEffect.searchImageDataURL;
    var smaaEffect = new POSTPROCESSING.SMAAEffect(searchImage,areaImage,1);

    this.godraysEffect = new POSTPROCESSING.GodRaysEffect(demo.camera, circle, {
      resolutionScale: 1,
      density: 0.8,
      decay: 0.92,
      weight: 0.3,
      samples: 100
    });

    var renderPass = new POSTPROCESSING.RenderPass(this.scene, demo.camera);
    var effectPass = new POSTPROCESSING.EffectPass(demo.camera,this.godraysEffect);
    effectPass.renderToScreen = true;
    this.composer = new POSTPROCESSING.EffectComposer(demo.renderer);
    this.composer.addPass(renderPass);
    this.composer.addPass(effectPass);

    this.godraysEffect.lightSource.scale.set(0.7,0.7,0.7)
    this.effectGrowSpeed = 0.0005

    this.logo_position_transition = new Transition(7.6, [this.logo.position.z, -440])
    this.logo_position_transition.start()
  }

  start(time){
    super.start(time)

    demo.camera.fov = 60;
    demo.camera.aspect = window.innerWidth/window.innerHeight;
    demo.camera.near = 1;
    demo.camera.far = 50000;
    demo.camera.position.y = 510;
    demo.camera.position.z = 200;
    demo.camera.updateProjectionMatrix();
  }

  onEvent(event, time, onStart){
    switch (event.tag) {
      case "scene0:hit_1":
      case "scene0:hit_2":
      case "scene0:hit_3":
      case "scene0:hit_4":
        this.camera_sake = true
        this.camera_pos_x = demo.camera.position.x
        this.camera_pos_y = demo.camera.position.y

        setTimeout(() =>{
          this.camera_sake = false
          demo.camera.position.x = this.camera_pos_x
        },300)
    break;
    }
    this.effectGrowSpeed = 0
    this.logoLight.visible = false
    if (event.tag === "scene0:pre") {
      this.effectGrowSpeed = 0.007
      this.logoLight.visible = true
    }

    if (event.tag === "scene0:hit_1")
      this.camera_move = true

    if (event.tag === "scene0:hit_2"){
      this.text1.visible = true
      this.logo.visible = false
    }

    if (event.tag === "scene0:hit_3"){
      this.text2.visible = true
      this.text1.visible = false
    }

    if (event.tag === "scene0:hit_4"){
      this.text3.visible = true
      this.text2.visible = false
    }
  }

  render(time, timeA) {
    this.logo.position.z = this.logo_position_transition.check(timeA)

    if (this.camera_sake)
      demo.camera.position.x = this.camera_pos_x + 30 * Math.random()

    if (this.logoLight.visible)
        this.logoLight.intensity += 0.03

    this.godraysEffect.lightSource.scale.x += this.effectGrowSpeed
    this.godraysEffect.lightSource.scale.y += this.effectGrowSpeed
    this.godraysEffect.lightSource.scale.z += this.effectGrowSpeed

    if (this.camera_move){
      function move(object, phase, divisor){
        object.position.x = object.position.x + Math.cos(time + phase) / 8
        object.position.y = object.position.y + Math.cos(time/2 + phase) / 8
      }

      move(this.logo, 0, 8)
      move(this.text1, -0.5, 8)
      move(this.text2, 0, 16)
      move(this.text3, 0, 8)
    }

    this.composer.render(0.1);

    return true;
  }

}
