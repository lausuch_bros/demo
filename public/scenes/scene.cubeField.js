class CubeFieldScene extends Scene{
  // preload(sync){
  //   sync.addTask((sync) =>{
  //     new THREE.TextureLoader().load(
  //     	// resource URL
  //     	'data/metal.jpg',
  //
  //     	// onLoad callback
  //     	( texture ) =>{
  //         texture.anisotropy = demo.renderer.getMaxAnisotropy()
  //         this.metal_material = new THREE.MeshBasicMaterial( {
  //     			map: texture,
  //           wireframe: true,
  //           wireframeLinewidth:1
  //     		});
  //
  //         sync.ready()
  //     	},
  //
  //     	// onProgress callback currently not supported
  //     	undefined,
  //
  //     	// onError callback
  //     	function () {
  //     		console.error( 'An error happened.' );
  //     	}
  //     );
  //   })
  // }

  setup(){
    {
      const color = 0xFFFFFF;
      const intensity = 1;
      const light = new THREE.PointLight(color, intensity);
      light.position.set(-100, 100, -100);
      this.scene.add(light);
    }

    {
      const color = 0xFFFFFF;
      const intensity = 1;
      const light = new THREE.PointLight(color, intensity);
      light.position.set(100, 100, -100);
      this.scene.add(light);
      this.frontLight = light
    }


    var size = 5
    var count = 50
    var geometry = new THREE.BoxBufferGeometry( size, 20, size);
    //var material = new THREE.MeshPhongMaterial( { color: 0xA0A0FF, specular: 0x999900, shininess: 90, flatShading: false, wireframe: true, } )

    this.nboxes = count
    this.boxes = [count * count]
    this.speed = [count * count]

    for (var x=0; x<count; x++ )
      for (var y=0; y<count; y++ ){
        //var color = (0xFF * (1.0*x)/count)<<16 + (0xFF * (1.0*y)/count)<<8 + 0xFF
        var material = new THREE.MeshPhongMaterial( { color: 0xA0A0FF, specular: 0xF09900, shininess: 30, flatShading: false, wireframe: true } )

        var box = new THREE.Mesh( geometry, material );
        box.position.z = -500 + size * y
        box.position.y = -10 + Math.cos(3.14 * 100 * x / count) +  Math.cos(3.14 * 100 * y / count)
        box.position.x = size * x - size*count/2
        this.scene.add(box)

        this.boxes[x + y * count] = box
        this.speed[x + y * count] = 0
      }

    this.centerbox = this.boxes[Math.floor(count/2) + Math.floor(count*count/2)].position

    this.scene.fog = new THREE.FogExp2( 0x000030, 0.005 );

    var bloomPass = new THREE.UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 1.5, 0.4, 0.85 );
    bloomPass.threshold = 0;
    bloomPass.strength = 1;
    bloomPass.radius = 1.3;
    var composer = new THREE.EffectComposer( demo.renderer );
    composer.addPass( new THREE.RenderPass( this.scene, demo.camera ) );
    composer.addPass( bloomPass );

    this.bloomPass = bloomPass
    this.composer = composer

    this.low_transition = new Transition(1, [3, 1])
    this.hi_transition = new Transition(0.5, [1, 0])
    this.hi_transition_r = new Transition(0.5, [0xFF, 0xA0])
    this.hi_transition_g = new Transition(0.5, [0xFF, 0xA0])
    this.drop_transition = new Transition(0.5, [1, 5])
    this.amplitude_transition = new Transition(40.5, [1, 6])
    this.amplitude_transition.start()

    this.camera_rotation_transition = new Transition(40.5, [10, 2])
    this.camera_rotation_transition.start()
  }

  start(time){
    super.start(time)
    demo.camera.position.y = 60
  }

  onEvent(event, time, onStart){
    switch (event.tag) {
      case "scene1:piano_low_1":
      case "scene1:piano_low_2":
      case "scene1:piano_low_3":
        this.low_transition.start()
        this.hi_transition_r.start()
        break;

      case "scene1:piano_1_1":
      case "scene1:piano_1_2":
      case "scene1:piano_1_3":
      case "scene1:piano_1_4":
      case "scene1:piano_1_5":
      case "scene1:piano_1_6":
        this.hi_transition.start()
        this.hi_transition_g.start()
      break

      case "scene1:piano_1_7":
        this.hi_transition.start()
        this.hi_transition_r.start()
        this.hi_transition_g.start()
      break

      case "scene1:piano_2_1":
      case "scene1:piano_2_2":
      case "scene1:piano_2_3":
      case "scene1:piano_2_4":
      case "scene1:piano_2_5":
      case "scene1:piano_2_6":
      case "scene1:piano_2_7":
        this.bloomPass.radius = 0.3;
        this.hi_transition.start()
        this.hi_transition_g.start()
      break

      case "scene1:drop":
        this.isDrop = true
        this.drop_transition.start()
      break
    }
  }

  render(time){
    var random_x = this.nboxes * Math.random()
    var random_y = this.nboxes * Math.random()
    var camera_angle = time/this.camera_rotation_transition.check(time)

    var red = this.hi_transition_r.check(time)
    var green = this.hi_transition_g.check(time)
    var amplitude = this.amplitude_transition.check(time)

    for (var x=0; x<this.nboxes; x++ )
      for (var y=0; y<this.nboxes; y++ ){
        // var distance = (Math.sqrt((random_x-x) * (random_x-x) + (random_y - y) * (random_y -y))-this.nboxes)/(this.nboxes * 0.5)
        // this.speed[x + y * this.nboxes] = this.speed[x + y * this.nboxes] + distance * 10 - 1
        //this.boxes[x + y * this.nboxes].position.y = this.boxes[x + y * this.nboxes].position.y + this.speed[x + y * this.nboxes]
        this.boxes[x + y * this.nboxes].position.y = amplitude * (Math.cos(y/2+time) + Math.cos(y/4+time) + Math.sin(x/2+time) + Math.sin(x/4+time))
        //10*Math.cos(3.14 * 100 * x * time * 0.05 / this.nboxes) +  Math.cos(3.14 * 100 * y * time * 0.05 / this.nboxes)

        this.boxes[x + y * this.nboxes].material.color.r = red / 256
        this.boxes[x + y * this.nboxes].material.color.g = green / 256
      }



    demo.camera.position.x = this.centerbox.x + Math.cos(camera_angle)*100
    demo.camera.position.z = this.centerbox.z + Math.sin(camera_angle)*100
    demo.camera.lookAt( this.centerbox );

    this.frontLight.position.x = this.centerbox.x + Math.cos(camera_angle+3.14)*400
    this.frontLight.position.z = this.centerbox.z + Math.sin(camera_angle+3.14)*400

    if (!this.isDrop)
      this.bloomPass.strength = this.low_transition.check(time) + this.hi_transition.check(time)
    else
      this.bloomPass.strength = this.drop_transition.check(time)



    this.composer.render()
    return true
  }
}
