class Particles_Scene extends Scene{

    start(){
      super.start()

      demo.camera.fov = 45
      demo.camera.near = 0.1
      demo.camera.far = 1000
      demo.camera.position.x = 30;
      demo.camera.position.y = 0;
      demo.camera.position.z = 100;
      demo.camera.lookAt(new THREE.Vector3(10, 0, 0));
      demo.camera.updateProjectionMatrix();
    }

    setup(){
        var knot;

        this.radius = 60;
        this.tube = 18.2;
        this.radialSegments = 1000;
        this.tubularSegments = 12;
        this.p = 5;
        this.q = 4;
        this.heightScale = 5;
        this.asParticles = true;
        this.rotate = true;


        var torusGeom = new THREE.TorusGeometry(40, 50, 70, 40);

        var torusKnotGeom = new THREE.TorusKnotGeometry(
          this.radius,
          this.tube,
          Math.round(this.radialSegments),
          Math.round(this.tubularSegments),
          Math.round(this.p),
          Math.round(this.q),
          this.heightScale
        );

        var points = [];
        for ( var i = 0; i < 200; i ++ ) {
            points.push( new THREE.Vector2( Math.sin( i * 0.3 ) * Math.sin( i * 0.1 ) * 15 + 50, ( i - 5 ) * 2 ) );
        }
        var latheGeom = new THREE.LatheBufferGeometry( points, 50 )

        function generateSprite() {
          var canvas = document.createElement("canvas");
          canvas.width = 16;
          canvas.height = 16;

          var context = canvas.getContext("2d");
          var gradient = context.createRadialGradient(
            canvas.width / 2,
            canvas.height / 2,
            0,
            canvas.width / 2,
            canvas.height / 2,
            canvas.width / 2
          );
          gradient.addColorStop(0, "rgba(255,255,255,1)");
          gradient.addColorStop(0.2, "rgba(180,255,255,1)");
          gradient.addColorStop(0.4, "rgba(0,0,64,1)");
          gradient.addColorStop(1, "rgba(0,0,0,1)");

          context.fillStyle = gradient;
          context.fillRect(0, 0, canvas.width, canvas.height);

          var texture = new THREE.Texture(canvas);
          texture.needsUpdate = true;
          return texture;
        }

        function createParticleSystem(geom) {
          var material = new THREE.ParticleBasicMaterial({
            color: 0xffffff,
            size: 3,
            transparent: true,
            blending: THREE.AdditiveBlending,
            map: generateSprite()
          });

          var system = new THREE.ParticleSystem(geom, material);
          system.sortParticles = true;
          return system;
        }

        // Torus 1
        this.torus1 = createParticleSystem(torusGeom);
        this.torus1.visible = true
        this.scene.add(this.torus1);
        this.torus1.material.color.r = 0
        this.torus1.material.color.g = 0
        this.torus1.material.color.b = 1

        // Torus 2
        this.torus2 = createParticleSystem(torusGeom);
        this.torus2.rotation.x = 3.14/2;
        this.torus2.material.color.r = 0
        this.torus2.material.color.g = 1
        this.torus2.material.color.b = 0
        this.torus2.visible = false
        this.scene.add(this.torus2);


        // Torus 3
        this.torus3 = createParticleSystem(torusGeom);
        this.torus3.rotation.y = 3.14/2;
        this.torus3.material.color.r = 1
        this.torus3.material.color.g = 0
        this.torus3.material.color.b = 0
        this.torus3.visible = false
        this.scene.add(this.torus3);

        // TorusKnot
        this.torusKnot = createParticleSystem(torusKnotGeom);
        this.torusKnot.visible = false
        this.scene.add(this.torusKnot);

        // Lathe
        this.lathe = createParticleSystem(latheGeom);
        this.lathe.position.y = -40;
        this.lathe.position.z = 70;
        this.lathe.rotation.y = 3.14/2;
        this.lathe.material.color.r = 0
        this.lathe.material.color.g = 1
        this.lathe.material.color.b = 0
        this.lathe.visible = false
        this.scene.add(this.lathe);


        var bloomPass = new THREE.UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 1.5, 0.4, 0.85 );
        bloomPass.threshold = 0;
        bloomPass.strength = 1;
        bloomPass.radius = 1.3;
        var composer = new THREE.EffectComposer( demo.renderer );
        composer.addPass( new THREE.RenderPass( this.scene, demo.camera ) );
        composer.addPass( bloomPass );

        this.bloomPass = bloomPass
        this.composer = composer

        this.bloomPass.strength = 1

        this.rotationSpeed = 0.005;
        this.rotationX = false
        this.rotationY = true
        this.rotationZ = false

        this.positionX = false
        this.rotationY = true
        this.rotationZ = false

        this.glow = 3
        this.lastTorus = false
    }

    onEvent(event, time, onStart){
      this.bloomPass.strength += 0.001
      this.flash = true

      this.bloomPass.strength = 8
      setTimeout(() =>{
        this.flash = false
    },1000)

      switch (event.tag) {
        case "scene4:init":
            this.rotationSpeed = 0.01
        break

        case "scene4:1":
            this.rotationSpeed = -0.01
            this.rotationZ = true
        break

        case "scene4:2":
            this.rotationSpeed = 0.02

        break

        case "scene4:3":
            this.rotationSpeed = -0.02
            this.rotationX = true
        break

        case "scene4:4":
            this.rotationSpeed = 0.02
            this.torus2.visible = true
        break

        case "scene4:5":
            this.rotationSpeed = -0.02
        break

        case "scene4:6":
            this.torus3.visible = true
            this.rotationSpeed = 0.02
            this.rotationZ = true
        break

        case "scene4:7":
            this.rotationSpeed = -0.02
        break

        case "scene4:8":
            this.rotationSpeed = 0.005
            this.torus1.visible = false
            this.torus2.visible = false
            this.torus3.visible = false
            //this.torusKnot.visible = true
            this.rotationX = false
            this.rotationY = true
            this.rotationZ = false
            this.lathe.visible = true
            this.lastTorus = true
        break

        case "scene4:9":
            this.rotationSpeed = -0.005
            this.rotationZ = true
        break

        case "scene4:10":
            this.rotationSpeed = 0.01
            this.rotationX = true
        break

        case "scene4:11":
            this.rotationSpeed = -0.005
        break

        case "scene4:12":
            this.rotationSpeed = 0.02
            this.lathe.visible = true
            this.torusKnot.visible = true
            this.lathe.visible = false
            this.glow = 2

        break

        case "scene4:13":
            this.rotationSpeed = -0.02
        break

        case "scene4:14":
            this.rotationSpeed = 0.02
        break

        case "scene4:15":
            this.rotationSpeed = -0.02
        break

      }

    }

    render(time){
        if (this.rotationX) {
            this.torus1.rotation.x += this.rotationSpeed;
            this.torus2.rotation.x += this.rotationSpeed;
            this.torus3.rotation.x += this.rotationSpeed;
            if (this.lastTorus) {
                this.torusKnot.rotation.x += this.rotationSpeed;
                this.lathe.rotation.x += this.rotationSpeed;
            }
        }
        if (this.rotationY) {
            this.torus1.rotation.y += this.rotationSpeed;
            this.torus2.rotation.y += this.rotationSpeed;
            this.torus3.rotation.y += this.rotationSpeed;
            if (this.lastTorus) {
                this.torusKnot.rotation.y += this.rotationSpeed;
                this.lathe.rotation.y += this.rotationSpeed;
            }
        }
        if (this.rotationZ) {
            this.torus1.rotation.z += this.rotationSpeed;
            this.torus2.rotation.z += this.rotationSpeed;
            this.torus3.rotation.z += this.rotationSpeed;
            if (this.lastTorus) {
                this.torusKnot.rotation.z += this.rotationSpeed;
                this.lathe.rotation.z += this.rotationSpeed;
            }
        }
        this.torusKnot.material.color.r += 0.0001
        this.torusKnot.material.color.g += 0.0001
        this.torusKnot.material.color.b += 0.0004
        // this.torusKnot.position.z -= 0.1
        // this.torusKnot.position.x -= 0.1
        // this.torusKnot.position.y += 0.1

        if (this.flash) {
            this.bloomPass.strength -= 0.1;
        }
        else {
            //this.bloomPass.strength = 1
            this.bloomPass.strength = Math.abs(Math.sin(time*3.45)) * this.glow ;
        }




        this.composer.render()
        return true
    }
}
