class TextScene2 extends Scene{
  setup(){
    console.log("TextScene: Setup")
    var numParticles = 15000;
    this.numParticles = numParticles;

    var positions = new Float32Array( numParticles * 3 );
    var scales = new Float32Array( numParticles );

    var poriginal = new Float32Array( numParticles * 2 );
    this.poriginal = poriginal

    var pdestine1 = new Float32Array( numParticles * 2 );
    this.pdestine1 = pdestine1
    var pdestine2 = new Float32Array( numParticles * 2 );
    this.pdestine2 = pdestine2

    var destine_i1 = 0
    var destine_l1 = Text1.length

    var destine_i2 = 0
    var destine_l2 = Text2.length

    for (var i=0; i<numParticles; i++){
      positions[3*i] = Math.random()*300-150
      positions[3*i+1] = Math.random()*300-150
      positions[3*i+2] = -90

      poriginal[2*i] = positions[3*i]
      poriginal[2*i + 1] = positions[3*i + 1]

      pdestine1[2*i] = (Text1[destine_i1] % 640 - 640/2) /10
      pdestine1[2*i + 1] = -1 * (Text1[destine_i1] / 640 - 480/2) /10

      pdestine2[2*i] = (Text2[destine_i2] % 640 - 640/2) /10
      pdestine2[2*i + 1] = -1 * (Text2[destine_i2] / 640 - 480/2) /10

      destine_i1++
      if (destine_i1 == destine_l1)
        destine_i1 = 0

      destine_i2++
      if (destine_i2 == destine_l2)
        destine_i2 = 0

      scales[ i ] = 2;
    }

    this.geometry = new THREE.BufferGeometry()
    this.geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) )
    this.geometry.addAttribute( 'scale', new THREE.BufferAttribute( scales, 1 ) )

    var vertexshader=[
      "attribute float scale;",
      "varying vec4 v_positionWithOffset;",
      "void main() {",
        "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "gl_PointSize = scale * 0.8;", //* ( 300.0 / - mvPosition.z );
        "gl_Position = projectionMatrix * mvPosition;",
      "}"].join("\n")

    var fragmentshader=[
      "uniform vec3 color;",

      "void main() {",
        //"if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;",
        "gl_FragColor = vec4( color, 1.0 );",
        //"gl_FragColor = vec4(0.0078125, 0.82421875, 0.37109375, 1);",
      "}"].join("\n")

    this.material = new THREE.ShaderMaterial( {
      uniforms: {
        color: { value: new THREE.Color( 0xffffff ) },
      },
      vertexShader: vertexshader,
      fragmentShader: fragmentshader
    } )


    this.particles = new THREE.Points( this.geometry, this.material )
    this.particles.layers.set(1)
		this.scene.add( this.particles )

    this.state = undefined
    this.text = this.pdestine1

    // var lineGeometry = new THREE.BufferGeometry();
    // var points = [];
    // var point = new THREE.Vector3();
		// var direction = new THREE.Vector3();
    //
    // for ( var i = 0; i < 50; i ++ ) {
		// 	direction.x += Math.random() - 0.5;
		// 	direction.y += Math.random() - 0.5;
		// 	direction.z = -90
		// 	direction.normalize().multiplyScalar( 10 );
		// 	point.add( direction );
		// 	points.push( point.x, point.y, point.z );
		// }
    //
    // lineGeometry.addAttribute( 'position', new THREE.Float32BufferAttribute( points, 3 ) );
    // var line = new THREE.Line( lineGeometry );
    // this.scene.add( line );


  }

  render(time){
    //demo.music.stop()
    if (this.state === undefined){
      this.timeStart = time
      this.state = "0"
    }

    demo.timerController.check(time, (event, time, onStart) => {
      console.log(event.tag, onStart)
      switch (event.tag) {
        case "1":
          this.timeStart = time
          this.state = "1"

        break;
        case "1b":
          this.timeStart = time
          this.state = "1b"
          this.text = this.pdestine2
        break;
        case "2":
          this.state = "2"
          this.text = this.pdestine2
          this.timeStart = time

          // setTimeout(() =>{
          //    this.state = "1b"
          //    this.text = this.pdestine2
          // },100)
        break;
        case "3":
        this.timeStart = time
        this.state = "3"
        break
        case "4":
        case "5":


          break;
      }
    })

    var speed = 2
    console.log(this.state)
    switch (this.state) {
      case "0":
      //-Math.random()/100
          this.setPosition(speed, speed, time, this.timeStart)
        break;
      case "1":
          this.lastPos = speed-(time-this.timeStart)
          this.setPosition(speed-(time-this.timeStart), speed, time, this.timeStart)
        break;
      default:
      case "1b":
          this.setPosition(this.lastPos + (time-this.timeStart) , speed, time, this.timeStart)
        break;
      case "2":
          this.setPosition(speed-(time-this.timeStart), speed, time, this.timeStart)
        break;

    }


    // this.composer.render( 0.01 );
  }

  setPosition(pos, speed, time, timeStart){
    var positions = this.particles.geometry.attributes.position.array;
    var scales = this.particles.geometry.attributes.scale.array;

    if (pos<speed){
      var factor = pos*pos/(speed*speed)
      for ( var i = 0; i < this.numParticles; i ++ ) {
        positions[3*i + 0] = this.poriginal[2*i + 0] * (1-factor) + this.text[2*i + 0] * (factor)
        positions[3*i + 1] = this.poriginal[2*i + 1] * (1-factor) + this.text[2*i + 1] * (factor)
        scales[i] = 2 + factor * 2
      }

      this.particles.geometry.attributes.position.needsUpdate = true;
      this.particles.geometry.attributes.scale.needsUpdate = true;
    }
    else{
      var distorsion = 20-(time-timeStart)*20
      if (distorsion<0.5) distorsion = 0.5
      for ( var i = 0; i < this.numParticles; i ++ ) {
        positions[3*i + 0] = this.text[2*i + 0] + (Math.random()*distorsion-distorsion/2)
        positions[3*i + 1] = this.text[2*i + 1] //+ Math.random()*0.5
        scales[i] = 2
      }

      this.particles.geometry.attributes.position.needsUpdate = true;
    }
  }
}
