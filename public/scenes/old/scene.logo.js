class Lines extends Scene{
  setup(){
    this.scene.add( new THREE.AmbientLight( 0x404040 ) );
    this.lines = []
    this.angles = []
    this.in_radius = []
    var max = 100
    this.max = max

    for (var i = 0; i<max; i++){
      var lineGeometry = new THREE.BufferGeometry();
      var points = [];
      var direction = new THREE.Vector3();
      var angle = 3.14*(i*2-max)/max

      var in_radius = 100

      this.angles.push(angle)
      this.in_radius.push(in_radius)

      var point = new THREE.Vector3();
      direction.x = 10*Math.cos(angle)
  		direction.y = 10*Math.sin(angle)
  		direction.z = -1
  		point.add( direction );
  		points.push( point.x, point.y, point.z );

      direction.x = in_radius*Math.cos(angle)
  		direction.y = in_radius*Math.sin(angle)

      direction.z = -60

      var point = new THREE.Vector3();
  		point.add( direction );
  		points.push( point.x, point.y, point.z );

      lineGeometry.addAttribute( 'position', new THREE.Float32BufferAttribute( points, 3 ) );
      var line = new THREE.Line( lineGeometry );
      this.lines.push(line)
      this.scene.add(line);
    }
  }

  render(time){
    var line = this.lines[0]
    var max = this.max
    var max_in_radius = 5

    var i = 0
    this.lines.some((line) =>{
      var positions = line.geometry.attributes.position.array;
      var angle = this.angles[i] + time / 5 + (time/10 + Math.random()/100)
      var in_radius = this.in_radius[i] - time/1
      if (in_radius < max_in_radius)
        in_radius = max_in_radius

      this.in_radius[i] = in_radius
      positions[0] = 10*Math.cos(angle)
      positions[1] = 10*Math.sin(angle)
      positions[3] = in_radius * Math.cos(angle)
      positions[4] = in_radius * Math.sin(angle)

      line.geometry.attributes.position.needsUpdate = true;
      i++
    })


    //this.composer.render();
    return true
  }
}

class LogoScene extends Scene{
  preload(sync){
    sync.addTask((sync) =>{
      new THREE.TextureLoader().load(
      	// resource URL
      	'data/logo.png',

      	// onLoad callback
      	( texture ) =>{
          this.logo_material = new THREE.MeshBasicMaterial( {
      			map: texture,
            transparent: true,
      		});

          sync.ready()
      	},

      	// onProgress callback currently not supported
      	undefined,

      	// onError callback
      	function () {
      		console.error( 'An error happened.' );
      	}
      );
    })
  }

  setup(){
    console.log("Material", this.logo_material)
    var plane = new THREE.Mesh(
      new THREE.PlaneGeometry( 1, 1, 1 ),
      this.logo_material );

    plane.material.side = THREE.DoubleSide;
    plane.layers.set(2)
    this.plane = plane
    this.scene.add( plane );
  }

  render(time){
    //this.plane.rotation. = time
  }

}

class ComposeScene extends Scene{
  constructor(){
    super()

    this.lines = new Lines()
    this.text = new TextScene2()
    this.logo = new LogoScene()
  }

  preload(sync){
    this.lines.preload(sync)
    this.text.preload(sync)
    this.logo.preload(sync)
  }

  setup(){
    this.lines.setup()
    this.text.setup()
    this.logo.setup()

    var bloomPass = new THREE.UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 1.5, 0.4, 0.85 );
    bloomPass.threshold = 0;
    bloomPass.strength = 6;
    bloomPass.radius = 0.5;
    var composer = new THREE.EffectComposer( demo.renderer );
    composer.addPass( new THREE.RenderPass( this.lines.scene, demo.camera ) );
    composer.addPass( bloomPass );

    this.composer = composer
  }

  render(time){
    demo.renderer.clear();
    demo.camera.layers.set(0);

    this.lines.render(time);
    this.composer.render()

    demo.camera.layers.set(2);
    demo.renderer.clearDepth();
    //this.text.render(time)
    this.logo.render(time)

    demo.renderer.render(this.logo.scene, demo.camera);

    return true
  }
}
