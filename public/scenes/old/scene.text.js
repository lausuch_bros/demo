class TextScene extends Scene{
  setup(){
    console.log("TextScene: Setup")
    var numParticles = 1000;
    this.numParticles = numParticles;
    var positions = new Float32Array( numParticles * 3 );
    var scales = new Float32Array( numParticles );

    for (var i=0; i<numParticles; i++){
      positions[3*i] = Math.random()*100-50
      positions[3*i+1] = Math.random()*100-50
      positions[3*i+2] = -90
      scales[ i ] = 1;
    }

    this.geometry = new THREE.BufferGeometry()
    this.geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) )
    this.geometry.addAttribute( 'scale', new THREE.BufferAttribute( scales, 1 ) );

    this.material = new THREE.ShaderMaterial( {
      uniforms: {
        color: { value: new THREE.Color( 0xffffff ) },
      },
      vertexShader: document.getElementById( 'vertexshader' ).textContent,
      fragmentShader: document.getElementById( 'fragmentshader' ).textContent
    } )


    this.particles = new THREE.Points( this.geometry, this.material )
		this.scene.add( this.particles )

    // demo.camera.position.x = 0
		// demo.camera.position.y = 0
    // demo.camera.position.z = 0
    // demo.camera.lookAt( this.scene.position );
    //
    // var renderModel = new THREE.RenderPass( this.scene, demo.camera );
    // var effectBloom = new THREE.BloomPass( 0.75 );
    // var effectFilm = new THREE.FilmPass( 0.5, 0.5, 1448, false );
    // var effectFocus = new THREE.ShaderPass( THREE.FocusShader );
    // effectFocus.uniforms[ "screenWidth" ].value = window.innerWidth * window.devicePixelRatio;
    // effectFocus.uniforms[ "screenHeight" ].value = window.innerHeight * window.devicePixelRatio;
    //
    // var composer = new THREE.EffectComposer( demo.renderer );
    // composer.addPass( renderModel );
    // composer.addPass( effectBloom );
    // composer.addPass( effectFilm );
    // composer.addPass( effectFocus );
    // this.composer = composer
    //
    // composer.setSize( window.innerWidth, window.innerHeight );

    console.log(this.scene.position)

    this.speed = 1.001
  }

  render(time){
    //demo.music.stop()

    demo.timerController.check(time, (event, time, onStart) => {
      console.log(event.tag, onStart)
      switch (event.tag) {
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
          this.speed = 0.98

          setTimeout(() =>{
            this.speed = 1.001
          },100)

          break;
      }
    })

    var positions = this.particles.geometry.attributes.position.array;

    for ( var i = 0; i < this.numParticles; i ++ ) {
      positions[3*i + 0] = positions[3*i + 0] / this.speed
      positions[3*i + 1] = positions[3*i + 1] / this.speed
    }

    this.particles.geometry.attributes.position.needsUpdate = true;

    // this.composer.render( 0.01 );
  }
}
