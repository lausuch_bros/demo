class CubeScene extends Scene{
  setup(){
    console.log("Scene: Setup")
    this.rotate_cube = false
    this.accel = 1
    this.camera_shake = false
    this.camera_current_pos = {x:0,y:0}

    {
      const color = 0xFFFFFF;
      const intensity = 1;
      const light = new THREE.DirectionalLight(color, intensity);
      light.position.set(-1, 2, 4);
      light.layers.enable( 1 );
      this.scene.add(light);
    }

    const boxWidth = 1;
    const boxHeight = 1;
    const boxDepth = 1;
    const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);

    const material = new THREE.MeshPhongMaterial({color: 0x44aa88});  // greenish blue

    this.cube = new THREE.Mesh(geometry, material);

    this.cube.layers.set(1)
    this.scene.add(this.cube)
    console.log(this.cube.layers)

    this.rotate_cube = true


    // demo.timerController.add_callback(["1","2","3","4","5"], (event,time,onStart) =>{
    //   //demo.music.stop()
    //   var white = new THREE.Color( 0xffffff );
    //   var black = new THREE.Color( 0x0 );
    //   var inverted = false
    //
    //   var effect = new IntervalController(10, 10, (iter)=>{
    //     if (iter % 2)
    //       this.scene.background = white
    //     else
    //       this.scene.background = black
    //   })
    //   effect.start()
    // })
  }

  render(time){
    demo.timerController.check(time, (event, time, onStart) => {
      console.log(event.tag, onStart)
      switch (event.tag) {
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
          this.rotate_cube = true
          this.accel ++

          this.camera_shake = true
          this.camera_current_pos.x = demo.camera.position.x
          this.camera_current_pos.y = demo.camera.position.y

          setTimeout(() =>{
            this.camera_shake = false
            demo.camera.position.x = this.camera_current_pos.x
            demo.camera.position.y = this.camera_current_pos.y
          },300)

          break;
      }
    })

    if (this.rotate_cube) {
      this.cube.rotation.x = time * this.accel;
      this.cube.rotation.y = time * this.accel;
    }

    if (this.camera_shake){
      demo.camera.position.x = this.camera_current_pos.x + this.accel*(Math.random()-0.5)/10
      demo.camera.position.y = this.camera_current_pos.y + this.accel*(Math.random()-0.5)/10
    }

    // return true
  }
}
