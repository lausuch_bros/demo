class Beat_Scene extends Scene{

    start(){
        super.start()

        demo.camera.fov = 80
        demo.camera.near = 1
        demo.camera.far = 10000
        demo.camera.position.x = 0;
        demo.camera.position.y = 0;
        demo.camera.position.z = 1000;
        demo.camera.updateProjectionMatrix();
    }

    setup(){
    {
        this.sceneLight = new THREE.DirectionalLight(0xffffff,0.5);
        this.sceneLight.position.set(0,0,1);
        this.scene.add(this.sceneLight);

    }
    {
        this.portalLight = new THREE.PointLight(0x211400, 20, 600, 1.7);
        this.portalLight.position.set(0,0,250);
        this.scene.add(this.portalLight);
    }

    var starsGeometry = new THREE.Geometry();
    for ( var i = 0; i < 1000; i ++ ) {
    	var star = new THREE.Vector3();
    	star.x = THREE.Math.randFloatSpread( 2000 );
    	star.y = THREE.Math.randFloatSpread( 2000 );
    	star.z = THREE.Math.randFloatSpread( 2000 );
    	starsGeometry.vertices.push( star );
    }

    var starsMaterial = new THREE.PointsMaterial( { color: 0xAAAAAA } );
    var starField = new THREE.Points( starsGeometry, starsMaterial );
    this.scene.add( starField );



    this.portalParticles = [];
    this.smokeParticles = [];

    var loader = new THREE.TextureLoader();
    loader.load("data/smoke.png", (texture) => {
        var portalGeo = new THREE.PlaneBufferGeometry(350,350);
        var portalMaterial = new THREE.MeshStandardMaterial({
            map:texture,
            transparent: true
        });
        var smokeGeo = new THREE.PlaneBufferGeometry(1000,1000);
        var smokeMaterial = new THREE.MeshStandardMaterial({
            map:texture,
            transparent: true
        });
        for(var p=880;p>250;p--) {
            var particle = new THREE.Mesh(portalGeo,portalMaterial);
            particle.position.set(
                0.5 * p * Math.cos((4 * p * Math.PI) / 180),
                0.5 * p * Math.sin((4 * p * Math.PI) / 180),
                0.1 * p
            );
            particle.rotation.z = Math.random() *360;
            this.portalParticles.push(particle);
            this.scene.add(particle);
        }
        for(var p=0;p<40;p++) {
            var particle = new THREE.Mesh(smokeGeo,smokeMaterial);
            particle.position.set(
                Math.random() * 1000-500,
                Math.random() * 400-200,
                25
            );
            particle.rotation.z = Math.random() *360;
            particle.material.opacity = 0.6;
            this.portalParticles.push(particle);
            this.scene.add(particle);
        }


    });
    this.clock = new THREE.Clock();


  }

  onEvent(event, time, onStart){
    switch (event.tag) {
      case "scene3:beat_1":
      case "scene3:beat_2":
      case "scene3:beat_3":
      case "scene3:beat_4":
      case "scene3:beat_5":
      case "scene3:beat_6":
      case "scene3:beat_7":
        this.heartbeat = true
        this.last_beat = false

        setTimeout(() =>{
            this.heartbeat = false
        },100);
        setTimeout(() =>{
            this.heartbeat = true
        },200);
        setTimeout(() =>{
          this.heartbeat = false
        },300);
    break;
    }
  }

  render(time){

    var multiplier1, multiplier2, cameraSpeed;
    if (this.heartbeat) {
        this.portalLight.power = 800;
        multiplier1 = -5;
        multiplier2 = -0.2;
        cameraSpeed = 15;
        this.portalLight.color.r = this.portalLight.color.r + 0.005;
        this.portalLight.color.g = this.portalLight.color.g + 0.003;
        this.portalLight.color.b = this.portalLight.color.b + 0.003;

    }
    else if (this.last_beat) {
        cameraSpeed = 0;
    }
    else {
        this.portalLight.power = 500;
        multiplier1 = 1.5;
        multiplier2 = 0.2;
        cameraSpeed = 0;
    }

    if (this.clock != "undefined") {
        var delta = this.clock.getDelta();
        this.portalParticles.forEach(p => {
            p.rotation.z -= delta *multiplier1;
        });
        this.smokeParticles.forEach(p => {
            p.rotation.z -= delta *multiplier1;
        });
    }

    this.smokeParticles.forEach(p => {
        p.position.z += cameraSpeed;
    });
    this.portalParticles.forEach(p => {
        p.position.z += cameraSpeed;
    });
    this.portalLight.position.z += cameraSpeed
  }
}
