class Tunnel{
  constructor(scene, length, z_deep, radius){
    this.scene = scene
    this.length = length
    this.z_deep = z_deep
    this.radius = radius
  }

  setup(){


    this.scene.fog = new THREE.FogExp2( 0x0, 0.004 );

    {
      var size = 10
      this.count = 10
      var geometry = new THREE.BoxBufferGeometry( size, size, size);
      var material = new THREE.MeshPhongMaterial( { color: 0xA0A0FF, specular: 0x999900, shininess: 0, flatShading: false} )

      this.boxes = []

      for (var a=0; a<this.count; a++ ){
        var angle = a*2*3.14/this.count - 3.14
        for (var z=0; z<this.length; z++ ){
          var box = new THREE.Mesh( geometry, material );
          box.position.z = -1 * z * size - this.z_deep
          this.scene.add(box)

          var geo = new THREE.EdgesGeometry( box.geometry ); // or WireframeGeometry
          var mat = new THREE.LineBasicMaterial( { color: 0x0000FF, linewidth: 2 } );
          var wireframe = new THREE.LineSegments( geo, mat );
          box.add( wireframe );

          this.boxes[z + a * this.length] = box
        }
      }
    }
  }

  render(time, desp, despA, animationType){
    if (animationType === 0){
      for (var a=0; a<this.count; a++ ){
        var angle = a*2*3.14/this.count - 3.14

        for (var z=0; z<this.length; z++ ){
          var box = this.boxes[z + a * this.length]
          var angle = a*2*3.14/this.count - 3.14
          box.position.y = this.radius * Math.sin(angle + (z+time)/3.14) + 10*Math.sin(z/10)
          box.position.x = this.radius * Math.cos(angle + (z+time)/3.14) + 10*Math.sin(z/10)
          box.position.z += despA
          box.rotation.z = (a-angle)*2*3.14/this.count

          box.children[0].material.color.setHex(
            (100 + z*100/this.length) <<16 | //Math.floor(256 * Math.abs(Math.sin(time + 0.12 + a/10))) << 16 |
            Math.floor(256 * Math.abs(Math.sin(time + a/10 + z))) <<8 |
            Math.floor(256 * Math.abs(Math.sin(time + 3.14 + a/10))))

          //box.rotation.y = time
        }
      }
    }
    else{
      var v = time - Math.floor(time)
      var scale = 1 + v - 0.5

      for (var a=0; a<this.count; a++ ){
        var angle = a*2*3.14/this.count - 3.14

        for (var z=0; z<this.length; z++ ){
          var box = this.boxes[z + a * this.length]
          box.scale.set(scale, scale, scale)
          var angle = a*2*3.14/this.count - 3.14
          box.position.y = this.radius * Math.sin(z + angle + (z+time)/3.14) + 10*Math.sin(z/10)
          box.position.x = this.radius * Math.cos(z + angle + (z+time)/3.14) + 10*Math.sin(z/10)
          box.position.z += despA
          box.rotation.z = -1 * (a-angle)*2*3.14

          box.children[0].material.color.setHex(
            Math.floor(256 * Math.abs(Math.sin(time + 0.12 + a/10))) << 16 |
            0xFF <<8 |
            //Math.floor(256 * Math.abs(Math.sin(time + a/10))) <<8 |
            Math.floor(256 * Math.abs(Math.sin(time + 3.14 + a/10))))
          //box.rotation.y = time
        }
      }
    }
  }
}

class Stars{
  constructor(scene){
    this.scene = scene
  }

  preload(sync){
    sync.addTask((sync) =>{
      new THREE.TextureLoader().load(
      	// resource URL
      	'data/circle.png',

      	// onLoad callback
      	( texture ) =>{
          this.points_material = new THREE.PointsMaterial( { size: 1, sizeAttenuation: true, map: texture, alphaTest: 0.5, transparent: true } );
				  this.points_material.color.setHSL( 0.5, 1, 1 );
          sync.ready()
      	},

      	// onProgress callback currently not supported
      	undefined,

      	// onError callback
      	function () {
      		console.error( 'An error happened.' );
      	}
      );
    })
  }

  setup(){
    this.particle_radius = 60
    this.number_particles = 2000

    var geometry = new THREE.BufferGeometry();
    var vertices = [];
    for ( var i = 0; i < this.number_particles; i ++ ) {
      var x = this.particle_radius * Math.random() - this.particle_radius/2;
      var y = this.particle_radius * Math.random() - this.particle_radius/2;
      var z = -400 * Math.random();
      vertices.push( x, y, z );
    }

    geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    var particles = new THREE.Points( geometry, this.points_material );
    this.scene.add( particles );
    this.particles = particles
  }

  render(time, desp, despA){
    var positions = this.particles.geometry.attributes.position.array;
    for ( var i = 0; i < this.number_particles; i ++ ) {
      positions[3 * i + 2] += despA
      if (desp < 3700 || (desp > 6950 && desp<8400))
        if (positions[3 * i + 2] > 200){
          positions[3 * i] = this.particle_radius * Math.random() - this.particle_radius/2;
          positions[3 * i + 1] = this.particle_radius * Math.random() - this.particle_radius/2;
          positions[3 * i + 2] = positions[3 * i + 2] - 400
        }
    }

    this.particles.geometry.attributes.position.needsUpdate = true;
  }
}

class Tunnel_Scene extends Scene{
  constructor(){
    super()
    this.tunnel = new Tunnel(this.scene, 315, 3950, 30)
    this.tunnel2 = new Tunnel(this.scene, 200, 8800, 30)
    this.stars = new Stars(this.scene)
  }

  preload(sync){
    this.stars.preload(sync)
  }

  setup_effect(){
    var bloomPass = new THREE.UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 1.5, 0.4, 0.85 );
    bloomPass.threshold = 0;
    bloomPass.strength = 2;
    bloomPass.radius = 1;
    this.bloomPass = bloomPass

    var renderPass = new THREE.RenderPass( this.scene, demo.camera )
    var composer = new THREE.EffectComposer( demo.renderer );

    composer.addPass( renderPass );
    composer.addPass( bloomPass );

    this.composer = composer
  }

  setup(){
    this.tunnel.setup()
    this.tunnel2.setup()
    this.stars.setup()

    this.speed_transition = new Transition(43.0, [10, 10000])
    this.speed_transition.start()
    this.reverse_speed_transition = new Transition(1, [10, 0])

    this.camera_rotation_1_transition = new Transition(1, [0,0.02])
    this.camera_rotation_2_transition = new Transition(1, [0.01,-0.04])
    this.camera_rotation_3_transition = new Transition(1, [-0.04, 0.03])
    this.camera_rotation_4_transition = new Transition(5, [0.03, 0])

    this.desp = 0
    this.desp_correction  = 0

    this.setup_effect()

    this.starsSpeed = 1
    this.cameraStop = false
  }

  onEvent(event, time, onStart){
    console.log(this.desp)
    switch (event.tag) {
      case "scene2:1":
        this.camera_rotation_step = 1
        this.camera_rotation_1_transition.start()
        this.starsSpeed = 2
      break

      case "scene2:1_2":
        this.camera_rotation_step = 2
        this.camera_rotation_2_transition.start()
        this.starsSpeed = 3
        console.log("HOLA")
      break

      case "scene2:2":
        this.camera_rotation_step = 1
        this.camera_rotation_2_transition.start()
        this.starsSpeed = 4
      break

      case "scene2:2_2":
        this.camera_rotation_step = 2
        this.camera_rotation_2_transition.start()
        this.starsSpeed = 4
      break

      case "scene2:drop":
        //this.reverse_speed_transition.start()
        this.cameraStop = true
        //this.desp_correction = 1000
        this.camera_rotation_3_transition.start()
        this.camera_rotation_step = 3
        this.starsSpeed = 50
        setTimeout(() =>{
            this.starsSpeed = 1.5
        },1500);
      break;

      case "scene2:rythm_1":
        this.cameraStop = false
      break

      case "scene2:break":
        this.camera_rotation_4_transition.start()
        this.camera_rotation_step = 4
      break


    }

  }

  start(time){
    super.start(time)
    demo.camera.position.set(0,0,0)
  }

  render(time, timeA){
    if (this.camera_rotation_step === 1)
      demo.camera.rotation.z += this.camera_rotation_1_transition.check(time)

    if (this.camera_rotation_step === 2)
      demo.camera.rotation.z += this.camera_rotation_2_transition.check(time)

    if (this.camera_rotation_step === 3)
      demo.camera.rotation.z += this.camera_rotation_3_transition.check(time)

    if (this.camera_rotation_step === 4)
      demo.camera.rotation.z += this.camera_rotation_4_transition.check(time)

    var lastDesp = this.desp
    if (!this.reverse_speed_transition.running)
      this.desp = timeA + this.speed_transition.check(time) - this.desp_correction
    else{
      this.desp -= this.reverse_speed_transition.check(time)
      this.desp_correction = timeA + this.speed_transition.check(time) - this.desp
    }

    this.tunnel.render(time, this.desp, this.desp-lastDesp, 0);
    this.tunnel2.render(time, this.desp, this.desp-lastDesp, 1);
    if (!this.cameraStop)
        this.stars.render(time, this.desp , this.starsSpeed)
    else {
        this.stars.render(time, this.desp, -1)
    }

    this.composer.render()
    return true
  }
}
