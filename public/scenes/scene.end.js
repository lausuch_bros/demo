class End_Scene extends Scene{

    start(){
      super.start()

      demo.camera.fov = 75
      demo.camera.near = 1
      demo.camera.far = 10000
      demo.camera.position.x = 30;
      demo.camera.position.y = 0;
      demo.camera.position.z = 1000;
      demo.camera.lookAt(new THREE.Vector3(10, 0, 0));
      demo.camera.updateProjectionMatrix();
    }

    setup(){
        var geometry = new THREE.CubeGeometry( 200, 200, 200 );
        var material = new THREE.MeshLambertMaterial( { color: 0xaa6666, wireframe: false } );
        this.mesh = new THREE.Mesh( geometry, material );
        this.cubeSineDriver = 0;

        var logoGeo = new THREE.PlaneGeometry(300,300);
        var textGeo = new THREE.PlaneGeometry(500,233);
        var logoTexture = THREE.ImageUtils.loadTexture('data/logo.png');
        var textTexture = THREE.ImageUtils.loadTexture('data/Hackweek_text.png');
        var logoMaterial = new THREE.MeshLambertMaterial({color: 0x00ffff, opacity: 1, map: logoTexture, transparent: true, blending: THREE.AdditiveBlending})
        var textMaterial = new THREE.MeshLambertMaterial({color: 0x00ffff, opacity: 1, map: textTexture, transparent: true, blending: THREE.AdditiveBlending})
        var logo = new THREE.Mesh(logoGeo,logoMaterial);
        var text = new THREE.Mesh(textGeo,textMaterial);
        logo.position.z = 900;
        logo.scale.set(0.4, 0.4, 2);
        logo.material.opacity = 0.2;

        text.position.z = 910;
        text.position.x += 20;
        text.scale.set(0.3, 0.3, 0.3);
        text.material.opacity = 1;


        this.scene.add(text);
        this.scene.add(logo);

        this.text = text
        this.logo = logo

        var light = new THREE.DirectionalLight(0xffffff,0.8);
        light.position.set(-1,0,1);
        this.scene.add(light);

        var smokeTexture = THREE.ImageUtils.loadTexture('data/Smoke-Element.png');
        var smokeMaterial = new THREE.MeshLambertMaterial({color: 0x00dddd, map: smokeTexture, transparent: true});
        var smokeGeo = new THREE.PlaneGeometry(300,300);
        var smokeParticles = [];


        for (var p = 0; p < 150; p++) {
            var particle = new THREE.Mesh(smokeGeo,smokeMaterial);
            particle.position.set(Math.random()*500-250,Math.random()*500-250,Math.random()*1000-100);
            particle.rotation.z = Math.random() * 360;
            this.scene.add(particle);
            smokeParticles.push(particle);
        }
        this.smokeParticles = smokeParticles;
        this.clock = new THREE.Clock();
    }

    render(time){
        var delta = this.clock.getDelta();
        var sp = this.smokeParticles.length;
        while(sp--) {
            this.smokeParticles[sp].rotation.z += (delta * 0.2);
        }
        this.mesh.rotation.x += 0.005;
        this.mesh.rotation.y += 0.01;
        this.cubeSineDriver += .01;
        this.mesh.position.z = 100 + (Math.sin(this.cubeSineDriver) * 500);
        this.logo.position.x = Math.sin(time*2.5)*2.4 + 25;
        this.logo.position.y = Math.sin(time*1.8)*2.6;
    }
}
