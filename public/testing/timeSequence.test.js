'use strict';

function test_basic(){
    const events = [
      {start:1, end:1, tag:"1"},
      {start:2, end:2, tag:"2"},
      {start:3, end:3, tag:"3"},]

    var tsc = new TimeSequenceController(events, function(event, time, isStart){
      console.log("!!", event, time, isStart)
    })

    console.log("Start")
    tsc.check(0)
    tsc.check(1)
    tsc.check(2)
    tsc.check(3)
    tsc.check(4)
}

function test_end(){
    const events = [
      {start:1, end:3, tag:"1"},
      {start:2, end:2, tag:"2"},
      {start:3, end:3, tag:"3"},]

    var tsc = new TimeSequenceController(events, function(event, time, isStart){
      console.log("!!", event, time, isStart)
    })

    console.log("Start")
    tsc.check(0)
    tsc.check(1)
    tsc.check(2)
    tsc.check(3)
    tsc.check(4)
}

// test_basic()
test_end()
