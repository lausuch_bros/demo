'use strict';

function test1_timeouts(){
  const eventController = new EventController()
  eventController.add(new Event(
    1,
    function(time){
      console.log("!!!! The time has to be 1 ?", time)
    }
  ))

  eventController.check(0)
  eventController.check(1)
  eventController.check(2)
}

function test_timeout_delay(){
  const eventController = new EventController()
  eventController.add(new Event(
    2,
    function(time){
      console.log("!!!! The time has to be 3 ?", time)
    },
    1,
    1
  ))

  eventController.check(0)
  eventController.check(1)
  eventController.check(2)
  eventController.check(3)
  eventController.check(4)
  eventController.check(5)
}

function test_interval_infinite(){
  const eventController = new EventController()
  eventController.add(new Event(
    2,
    function(time){
      console.log("!!!! The time has to be 2 or 4 ?", time)
    },
    0
  ))

  eventController.check(0)
  eventController.check(1)
  eventController.check(2)
  eventController.check(3)
  eventController.check(4)
  eventController.check(5)
}

function test_interval_count(){
  const eventController = new EventController()
  eventController.add(new Event(
    2,
    function(time){
      console.log("!!!! The time has to be 2 or 4 ?", time)
    },
    2
  ))

  eventController.check(0)
  eventController.check(1)
  eventController.check(2)
  eventController.check(3)
  eventController.check(4)
  eventController.check(5)
  eventController.check(6)
  eventController.check(7)
  eventController.check(8)
}



// test1_timeouts()
// test_timeout_delay()
// test_interval_infinite()
test_interval_count()
