class Timer{
  constructor(interval, callback, count = 0, delay = 0){
    this.interval = interval
    this.callback = callback
    this.count = count
    this.delay = delay
    this.infinite = count === 0
    this.iteration = 0

    this.timeStart = undefined
    this.timeout = undefined
  }

  check(time){
    if ( this.timeStart === undefined && time >= this.delay ){
      this.timeStart = time
      this.running = true
      this.timeout = this.timeStart + this.interval
    }

    // console.log("next timeout", this.timeout, "time", time, "running", this.running)

    if ( this.running ){
      if (this.timeout <= time){
        this.callback(time, this.iteration)
        this.count --
        this.iteration ++

        if ( this.infinite || this.count > 0){
          this.timeout = this.timeout + this.interval
        }else
          this.running = false
      }
    }
  }
}

class TimerController{
  constructor(){
    this.events = []
  }

  add(event){
    this.events.push(event)
  }

  add_timeout(timeout, callback){
    this.events.push(new Timer(timeout, callback))
  }

  add_interval(interval, callback){
    this.events.push(new Timer(interval, callback))
  }

  check(time){
    this.events.forEach((e) => {
      e.check(time)
    })
  }
}

class TimeSequenceController{
  constructor(sequence){
    this.sequence = sequence
    this.cursor = 0
    this.length = this.sequence.length
    this.to_check_end = []
    this.timeStart = undefined
    this.eventCallbacks = {}
    this.offset = 0
  }

  setOffset(offset){
    this.offset = offset
  }

  check(time, callback){
    if (this.timeStart === undefined)
      this.timeStart = time - this.offset

    time -= this.timeStart

    while (this.cursor < this.length && time >= this.sequence[this.cursor].start){
      if ( this.sequence[this.cursor].start >= this.offset) {
        var event = this.sequence[this.cursor]

        callback(event, time, true)
        this.call_callbacks(event, time, true)

        if (event.start !== event.end)
          this.to_check_end.push(event)
      }

      this.cursor ++
    }

    // var new_to_check_end = []
    // this.to_check_end.forEach((event) =>{
    //   if (event.end <= time){
    //     callback(event, time, false)
    //     this.call_callbacks(event, time, false)
    //   }
    //   else
    //     new_to_check_end.push(event)
    // })
    //
    // this.to_check_end = new_to_check_end
  }

  add_callback(tag, callback){
    if (Array.isArray(tag)){
      tag.forEach((tag_) =>{
        this.add_callback(tag_, callback)
      })
    }else{
      if( this.eventCallbacks[tag] === undefined)
        this.eventCallbacks[tag] = [callback]
      else
        this.eventCallbacks[tag].push(callback)
    }
  }

  call_callbacks(event, time, onStart){
    console.log("event: ", event.tag)
    if (this.eventCallbacks[event.tag] !== undefined)
      this.eventCallbacks[event.tag].forEach((callback) =>{
        callback(event, time, onStart)
      })
  }
}

class Sync{
  constructor(callback){
    this.count = 0
    this.callback = callback
    this.tasks = []
  }

  addTask(task){
    this.tasks.push(task)
    this.count ++
  }

  start(){
    this.tasks.forEach((task)=>{
      task(this)
    })
  }

  ready(){
    this.count --

    if (this.count === 0)
      this.callback()
  }
}

class State{
  constructor(name, config){
    this.name = name
    this.config = config
  }

  render(time){
    //NOTE: Implement
  }
}

class StateMachine{
  constructor(){
    this.states = {}
    this.transitions = {}

    this.init_state = new State("init")
    this.current_state = this.init_state
    this.add_state(this.init_state)
  }

  add_state(state){
    this.states[state.name] = state
  }

  add_transition(state_from, state_to, condition){
    if (this.transitions[state_from.name] === undefined)
      this.transitions[state_from.name] = []

    this.transitions[state_from.name].push({to: state_to, condition: condition})
  }

  onEvent(event, time, onStart){
    var transitions = this.transitions[this.current_state.name]
    transitions.some(function (transition){
      if (transition.condition(event, time, onStart)){
        this.current_state = transition.to
        return true
      }
    })
  }

  check(){
    //TODO: ???????
  }


}

class IntervalController{
  constructor(count, interval, callback){
    this.interval = interval
    this.callback = callback
    this.count = count
    this.iter = 0
  }

  start(){
    this.iter = 0
    this.intervalId = setInterval(()=>{
      this.callback(this.iter)

      if (this.iter === this.count){
        this.iter = 0
        clearInterval(this.intervalId)
        this.intervalId = undefined
      }
      else
        this.iter ++
    }, this.interval)
  }
}

class Transition{
  constructor(duration, values){
    this.duration = duration
    this.values = values
    this.startTime = undefined
    this.running = false
  }

  start(){
    this.startTime = undefined
    this.running = true
  }

  check(time){
    if (this.running == false)
      return this.values[1]

    if (this.startTime === undefined)
      this.startTime = time

    var diff = (time - this.startTime)/this.duration

    if (diff > 1){
      diff = 1
      this.running = false
    }

    // console.log(diff, this.startTime,  this.duration, this.values[0] * (1 - diff) + this.values[1] * diff)
    return (this.values[0] * (1 - diff) + this.values[1] * diff)
  }
}
