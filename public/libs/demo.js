class Demo{
  constructor(scenes){
    if ( WEBGL.isWebGLAvailable() === false ) {
      document.body.appendChild( WEBGL.getWebGLErrorMessage() );
    }

    window.demo = this

    this.scenes = scenes
    this.current_scene_pos = 0
    this.current_scene = scenes[0]

    this.renderer = new THREE.WebGLRenderer({antialias: true});
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(this.renderer.domElement)
    this.renderer.autoClear = false;

    this.audioLoader = new THREE.AudioLoader();
    this.musicListener = new THREE.AudioListener();
    this.music = new THREE.Audio( this.musicListener );
    this.timerController = new TimeSequenceController(Events)

    this.camera = new THREE.PerspectiveCamera( 33, window.innerWidth / window.innerHeight, 0.1, 500 );
    this.camera.position.z = 10;
    this.camera.add(this.musicListener);

    this.camera.layers.enable( 0 ); // enabled by default
		this.camera.layers.enable( 1 );
    this.camera.layers.enable( 2 );

    window.addEventListener( 'resize', ()=>{this.onWindowResize()}, false );

    this.preloaded = false
    this.initial_delay = undefined
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }

  setup(){
    this.scenes.forEach((scene) =>{
      scene.setup()
    })
  }

  preload(callback){
    var sync = new Sync(()=>{
      demo.preloaded = true
      callback()
    })

    this.scenes.forEach((scene) =>{
      scene.preload(sync)
    })

    //TODO: Call to all scenes preload
    sync.addTask((sync) =>{
      this.audioLoader.load( './data/music.mp3', ( buffer ) => {
        this.music.setBuffer( buffer );
        this.music.setLoop(false);
        this.music.setVolume(1);
        console.log("Demo: Music ready")
        sync.ready()
      })
    })

    sync.start()
  }

  start(){
    requestAnimationFrame(this.render);
  }

  render(time){
    requestAnimationFrame(demo.render);

    if (demo.initial_delay === undefined)
      demo.initial_delay = time * 0.001

    time = time * 0.001 - demo.initial_delay;

    demo.timerController.check(time, (event, time, onStart) => {
      console.log("Event", event.tag, time)
      demo.onEvent(event, time, onStart)
      demo.current_scene.onEvent(event, time, onStart)
    })

    demo.current_scene.start(time)

    if (!demo.current_scene._render(time, time - demo.current_scene.startTime))
      demo.renderer.render(demo.current_scene.scene, demo.camera);
  }

  onEvent(event, time, onStart){}

  next_scene(){
    this.transition()
    setTimeout(()=>{
      this.reset_camera()
      this.current_scene_pos ++
      this.current_scene = this.scenes[this.current_scene_pos]
    },400)

  }

  reset_camera(){
    demo.camera.fov = 33;
    demo.camera.aspect = window.innerWidth/window.innerHeight;
    demo.camera.near = 0.1;
    demo.camera.far = 500;
    demo.camera.position.x = 0;
    demo.camera.position.y = 0;
    demo.camera.position.z = 10;
    demo.camera.lookAt(0,0,0)
    demo.camera.updateProjectionMatrix();
  }

  transition(){
    var div = demo.renderer.context.canvas
    div.classList.remove("screen-change");
    div.offsetWidth;
    div.classList.add("screen-change");
  }
}

class Scene{
  constructor(){
    this.scene = new THREE.Scene();
    this.scene_started = false
    this.startTime = 0
  }

  setup(){
    //NOTE: Implement
  }

  start(time){
    if (this.scene_started === false){
      this.startTime = time
      this.scene_started = true
    }
  }

  onEvent(event, time, onStart){

  }

  preload(sync){
    //NOTE: Implement
  }

  render(time, rtime){
    //NOTE: Implement
  }

  _render(time){
    return this.render(time, time - this.startTime)
  }
}

class SceneObject{
  setup(){
    //NOTE: Implement
  }

  preload(sync){
    //NOTE: Implement
  }

  render(time){
    //NOTE: Implement
  }
}
