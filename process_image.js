const fs = require('fs');
var PNG = require('png-js');

PNG.decode('data/text1.png', function(pixels) {
    // pixels is a 1d array of decoded pixel data
    //console.log(pixels.length)
    var vertex = []
    var len = 640*480
    var output = "var Text1=["

    for (var i=0; i<len; i++){
      var color = ""+pixels[4*i]+pixels[4*i + 1]+pixels[4*i + 2]+pixels[4*i + 3]
      if (color!="255255255255"){
        vertex.push(i)
        output += i + ","
      }
    }

    output += "]"


    fs.writeFile('./public/data/text1.js', output, (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
});

PNG.decode('data/text2.png', function(pixels) {
    // pixels is a 1d array of decoded pixel data
    //console.log(pixels.length)
    var vertex = []
    var len = 640*480
    var output = "var Text2=["

    for (var i=0; i<len; i++){
      var color = ""+pixels[4*i]+pixels[4*i + 1]+pixels[4*i + 2]+pixels[4*i + 3]
      if (color!="255255255255"){
        vertex.push(i)
        output += i + ","
      }
    }

    output += "]"


    fs.writeFile('./public/data/text2.js', output, (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
});
